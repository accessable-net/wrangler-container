FROM node:lts-alpine

ARG INCLUDE_WORKER_TEMPLATE=https://github.com/cloudflare/worker-template
ENV USER=node

COPY ./src/npm/* /home/node/

USER node
WORKDIR /home/node/app

RUN cd /home/node && \
    npm install --cache=/home/node/.cache && \
    mkdir -p /home/node/wrangler
RUN cd /home/node && /home/node/.wrangler/bin/wrangler generate worker "${INCLUDE_WORKER_TEMPLATE}" 
RUN cd "/home/node/worker" && npm install --cache=/home/node/.cache 
RUN cd /home/node/worker && /home/node/.wrangler/bin/wrangler build

RUN mkdir /home/node/.bin
ENV PATH=/home/node/.bin:$PATH
RUN ln -s /home/node/.wrangler/bin/wrangler ~/.bin/

CMD ["/home/node/.wrangler/bin/wrangler"]
